// Bài 1: Đề: Tìm số nguyên dương nhỏ nhất sao cho 1 + 2 + 3 + ... + n > 10000
function ketQuaB1(){
  var soNguyenDuongCanTimB1 = 1;
  for(var tongNB1=0; tongNB1<10000; soNguyenDuongCanTimB1++){
    tongNB1 += soNguyenDuongCanTimB1;
    document.getElementById('showKetQuaB1').innerHTML = `Số n là: ${soNguyenDuongCanTimB1}`;
  }


}

// Bài 2: Viết chương trình nhập vào 2 số x, n tính tổng: S(n) = x + x^2
// + x^3 + ... + x^n
function ketQuaB2(){
  var soXB2 = document.getElementById('soXB2').value*1;
  var soNB2 = document.getElementById('soNB2').value*1;
  var tongB2 = 0; 

for(var n = 1; n <= soNB2; n++){
  tongB2 += Math.pow(soXB2,n)
}
document.getElementById('showKetQuaB2').innerHTML = `Tổng là ${tongB2}`;
}

// Bài 3: Tính giai thừa 1*2*...n
function ketQuaB3(){
  var numberB3 = document.getElementById('numberB3').value*1;
  var giaiThuaB3 = 1;
  for(var n = 1; n <= numberB3; n++){ 
    giaiThuaB3 *= n;
  }
  document.getElementById('showKetQuaB3').innerHTML = giaiThuaB3;
}

// Bài 4: In thẻ Div Chẵn Lẻ
function ketQuaB4(){
  var outputString = '';
  for(var n=1; n <= 10; n++){
    var newDivLe = `<p class="bg-primary text-white">Div lẻ ${n}</p>`;
    var newDivChan = `<p class="bg-danger text-white">Div chẵn ${n}</p>`;
    if(n % 2 != 0){
      outputString += newDivLe;
    }else{
      outputString += newDivChan;
    }
  }
  document.getElementById('showKetQuaB4').innerHTML = outputString;
}